import Login from './src/screens/Login'
import { createStackNavigator, createAppContainer, createDrawerNavigator } from 'react-navigation'
import MyMap from './src/screens/Map'
import MapMarkerView from './src/screens/MapMarkerView'
import Categories from './src/screens/Categories'
import History from './src/screens/History';
import IasiDetails from './src/screens/IasiDetails'

const Stack = createStackNavigator({
    Login: { screen: Login },
    Map: { screen: MyMap },
    MapMarkerView: { screen: MapMarkerView },
    Categories: { screen: Categories },
    History: {
        screen: History
    },
    IasiDetails: { screen: IasiDetails }
})

export default createAppContainer(Stack);