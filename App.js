import React, { Component, Fragment } from 'react';

import { Provider } from 'mobx-react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import Stack from './MyNavigator'
import firebase from 'firebase'
import { initializeFirebase } from './src/config/config'
import markerStore from './src/stores/MarkerStore';
import { observer } from 'mobx-react'
// sha 1 da39a3ee5e6b4b0d3255bfef95601890afd80709

initializeFirebase()
@observer
export default class App extends Component {

  render() {
    return (
      <Provider store={markerStore}>
        <React.Fragment>
          <Stack />
        </React.Fragment>
      </Provider>
    )
  }
}