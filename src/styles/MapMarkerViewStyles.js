import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    details: {
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 20,
        color: '#fff',
        paddingHorizontal: 15
    },

    buttonsView: {
        flexDirection: 'row',
        justifyContent: "space-around",
        width: '100%',
        marginVertical: 10,
    },

    buttons: {
        width: '35%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: '#6FC7B6'
    },

    buttonsText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 15
    }
})