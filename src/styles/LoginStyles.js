import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        // justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },

    loginButton: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '70%',
        height: 50,
        // position: 'absolute',
        // bottom: 15,
        backgroundColor: '#fff',
        opacity: 0.7,
        borderRadius: 25,
        marginVertical: 5
    },

    loginButtonText: {
        color: '#000000',
        fontWeight: 'bold',
        fontSize: 20
    },

    modal1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
    },

    modal2: {
        width: '70%',
        height: '50%',
        backgroundColor: 'rgba(0,0,0,0.8)',
        justifyContent: 'center',
        alignItems: 'center'
    },

    modalButtonText: {
        color: "white",
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 10
    },

    input: {
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        width: '80%',
        height: 40,
        marginBottom: 10,
        fontSize: 15,
        color: '#fff'
    },

    modalButton: {
        width: '80%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

