import { StyleSheet, Dimensions } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },

    map: {
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        position: 'absolute'
    },

    radius: {
        height: 50,
        width: 50,
        borderRadius: 25,
        overflow: 'hidden',
        backgroundColor: 'rgba(0,122,255,0.1)',
        borderWidth: 1,
        borderColor: 'rgba(0,112,255,0.3)',
        alignItems: 'center',
        justifyContent: 'center'
    },

    marker: {
        height: 20,
        width: 20,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 10,
        overflow: 'hidden',
        backgroundColor: '#007AFF'
    },

    menuView: {
        backgroundColor: 'white',
        width: 200,
        // height: 250,
        position: 'absolute',
        top: 60,
        left: 60,
        borderColor: '#d9e3f0',
    },
    clicked: {
        height: 50,
        width: 50,
        borderRadius: 25,
        overflow: 'hidden',
        backgroundColor: 'rgba(255,0,0,0)',
        borderWidth: 1,
        borderColor: 'rgba(255,0,0,0.3)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    myLocation: {
        height: 50,
        width: 50,
        borderRadius: 25,
        overflow: 'hidden',
        backgroundColor: 'rgba(0,122,255,0.1)',
        borderWidth: 1,
        borderColor: 'rgba(0,112,255,0.3)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    myLocation2: {
        height: 20,
        width: 20,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 10,
        overflow: 'hidden',
        backgroundColor: '#007AFF'
    },

    modalButton: {
        width: 40,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        backgroundColor: '#fff',
        top: 30,
        left: 30,
        borderRadius: 20,
        position: 'absolute',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    myLocationButton: {
        width: 40,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        backgroundColor: '#fff',
        bottom: 70,
        right: 20,
        borderRadius: 20,
        position: 'absolute',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },

    clearButton: {
        backgroundColor: 'white',
        width: 40,
        height: 40,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        right: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    historyButton: {
        position: 'absolute',
        top: 40,
        width: 40,
        height: 40,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        right: 40,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    distanceView: {
        zIndex: 22,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 5,
        paddingHorizontal: 10,
        position: 'absolute',
        left: 40,
        bottom: 20,
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    saveRouteButton: {
        zIndex: 2,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 40,
        bottom: 20,
        right: 60,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    modal1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    modal2: {
        width: '70%',
        height: '50%',
        backgroundColor: 'rgba(0,0,0,0.8)',
        justifyContent: 'center',
        alignItems: 'center'
    },

    input: {
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        width: '80%',
        height: 40,
        marginBottom: 10,
        fontSize: 15,
        color: '#fff'
    },

    modalButton2: {
        width: '80%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },

    modalButtonText: {
        color: "white",
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 10
    },
});

