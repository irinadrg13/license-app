import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    itemView: {
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        height: 50,
        width: '100%',
        backgroundColor: '#fff',
        borderBottomColor: '#f4f4f4',
        borderBottomWidth: 1,
        alignItems: "center",
        flexDirection: 'row'
    }
})