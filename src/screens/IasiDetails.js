import React, { Fragment } from 'react'
import { View, Text, ScrollView, TouchableOpacity, Linking, ActivityIndicator } from 'react-native'
import 'firebase/firestore'
import firebase from 'firebase'
import { initializeFirebase } from '../config/config'

export default class IasiDetails extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            details: '',
            link: ''
        }
    }
    static navigationOptions = ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
        swipeEnabled: false
    })

    componentDidMount = () => {
        db = firebase.firestore()
        var docRef = db.collection("Iasi").doc("details");

        docRef.get().then((doc) => {
            if (doc.exists) {
                this.setState({ details: doc.data().description, link: doc.data().link })
            } else {
                console.log("No such document!");
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });
    }

    render() {
        console.log(this.state.details);

        return (
            <View style={{ flex: 1, paddingTop: 30, backgroundColor: 'rgba(0,0,0,0.7)' }}>
                <ScrollView>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ fontSize: 18, marginLeft: 15, height: 40, width: 60, borderRadius: 20, backgroundColor: 'rgba(255,255,255,0.7)', justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                        <Text>Go Back</Text>
                    </TouchableOpacity>
                    {
                        this.state.details !== '' ? (
                            <Fragment>
                                <Text style={{ color: '#fff', fontSize: 20, paddingHorizontal: 15, alignSelf: 'center' }}>{this.state.details}</Text>
                                <TouchableOpacity onPress={() => Linking.openURL(this.state.link)} style={{ fontSize: 18, height: 40, width: '80%', borderRadius: 20, backgroundColor: '#6FC7B6', justifyContent: 'center', alignItems: 'center', marginVertical: 10, alignSelf: 'center' }}>
                                    <Text>Read More about Iasi</Text>
                                </TouchableOpacity>
                            </Fragment>
                        ) : (
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator />
                                </View>
                            )
                    }
                </ScrollView>
            </View>
        )
    }
}