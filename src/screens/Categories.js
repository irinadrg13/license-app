import React, { Component } from 'react'
import { View, FlatList, Text, TouchableOpacity } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import styles from '../styles/CategoryStyles'
import { inject, observer } from 'mobx-react/native';


@inject('store')
@observer

export default class Categories extends Component {
    constructor(props) {
        super(props)
        this.state = {
            categories: []
        }
    }
    static navigationOptions = ({ navigation }) => ({
        title: 'Categories',
        headerLeft: (
            <TouchableOpacity
                style={{ height: '100%', width: 50, marginLeft: 10, justifyContent: 'center', alignItems: 'center' }}
                onPress={() => {
                    navigation.goBack();
                }}>
                <Ionicons name="ios-arrow-back" size={25} />
            </TouchableOpacity>
        )
    });

    componentDidMount() {
        setTimeout(
            () => {
                this.setState({
                    categories: [
                        { id: "1", name: "Airports", key: "airport" },
                        { id: "2", name: "Art galleries", key: "art_gallery" },
                        { id: "3", name: "Bus stations", key: "bus_station" },
                        { id: "4", name: "City Hall", key: "city_hall" },
                        { id: "5", name: "Libraries", key: "library" },
                        { id: "6", name: "Movie theaters", key: "movie_theater" },
                        { id: "7", name: "Museums", key: "museum" },
                        { id: "8", name: "Parks", key: "park" },
                        { id: "9", name: "Restaurants", key: "restaurant" },
                        { id: "10", name: "Stadium", key: "stadium" }
                    ]
                })
            }, 0
        )
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity style={styles.itemView}
                onPress={() => {
                    this.props.store.getMarkers({
                        type: item.key, succes: () => {
                            console.log("success")
                        }
                    });
                    this.props.navigation.navigate("Map")
                }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{item.name}</Text>
                <Ionicons name="ios-arrow-forward" size={18} />
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    renderItem={this.renderItem}
                    extraData={this.state}
                    keyExtractor={item => item.id}
                    data={this.state.categories}
                />
            </View>
        )
    }
}
