import React, { Component } from 'react'
import { inject, observer } from 'mobx-react/native';
import { View, Text, TouchableOpacity } from 'react-native'
import styles from '../styles/HistoryStyles'
import { FlatList } from 'react-native-gesture-handler';
import Geocoder from 'react-native-geocoding'
import 'firebase/firestore'
import firebase from 'firebase'
import { Ionicons, FontAwesome } from '@expo/vector-icons'
import MaterialIcons from '@expo/vector-icons/MaterialIcons';


@inject('store')
@observer
export default class History extends Component {
    constructor(props) {
        super(props)

        this.state = {
            routes: []
        }
    }

    static navigationOptions = ({ navigation }) => ({
        title: 'History',
        headerLeft: (
            <TouchableOpacity
                style={{ height: '100%', width: 50, marginLeft: 10, justifyContent: 'center', alignItems: 'center' }}
                onPress={() => {
                    navigation.goBack();
                }}>
                <Ionicons name="ios-arrow-back" size={25} />
            </TouchableOpacity>
        )
    })

    componentDidMount() {
        db = firebase.firestore()

        db.collection("routes").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                this.setState({
                    routes: [...this.state.routes, { id: doc.id, route: doc.data().route, name: doc.data().name, description: doc.data().description }]
                })
            })

        }).catch(e => console.log(e, "error snap"))
    }

    locationsName = (item) => {
        var locationName = Geocoder.from({
            latitude: item.latitude,
            longitude: item.longitude
        });
        this.setState({ name: locationName })
    }
    deleteRoute = (id) => {
        db = firebase.firestore()
        console.log(id, "dada id")
        db.collection("routes").doc(id).delete().then(e => {
            this.setState({
                routes: this.state.routes.filter(route => route.id !== id)
            })
        }).catch(e => console.log(e, "ondelete"))
    }

    renderItem = ({ item }) => {
        return (
            <View style={styles.itemView}>
                <View style={{ width: '60%' }}>
                    <Text>Route name: {item.name}</Text>
                    <Text>Route description: {item.description}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => {
                        this.props.store.viewRoute(item.route)
                        this.props.navigation.navigate("Map")
                    }}
                        style={styles.buttonsStyle}
                    >
                        <FontAwesome name="location-arrow" color="white" size={20} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.buttonsStyle}
                        onPress={() => { this.deleteRoute(item.id) }}
                    >
                        <MaterialIcons name="close" size={20} color="#ce0000" />
                    </TouchableOpacity>
                </View>
            </View>

        )
    }

    render() {
        return (
            <View style={styles.container} >
                <FlatList
                    data={this.state.routes}
                    keyExtractor={item => item.id}
                    renderItem={this.renderItem}
                    extraData={{ navigate: this.props.navigation, store: this.props.store }}
                />
            </View>
        )
    }
}