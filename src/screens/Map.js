import React, { Component } from 'react';
import { Dimensions, View, Text, TouchableOpacity, Modal, TextInput } from 'react-native';
import { Audio } from 'expo'
import MapView, { Marker } from 'react-native-maps';
import { inject, observer } from 'mobx-react/native';
import { Ionicons, SimpleLineIcons } from '@expo/vector-icons'
import styles from '../styles/MapStyles'
import 'firebase/firestore'
import firebase from 'firebase'
import { saveRouteModal, SaveRouteModal } from '../components/SaveRouteModal'

import MapViewDirections from 'react-native-maps-directions';
import MaterialIcons from '@expo/vector-icons/MaterialIcons';
// import { checkIfStateModificationsAreAllowed } from 'mobx/lib/internal';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 47.1593295
const LONGITUDE = 27.5674332
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const GOOGLE_MAPS_APIKEY = 'AIzaSyCTRVGqebWdSAUYdBP9jXYgSfIewJMyibM';
const MY_POSITION_LATITUDE = 0.01
const MY_POSITION_LONGITUDE = MY_POSITION_LATITUDE * ASPECT_RATIO
@inject('store')
@observer

export default class Map extends Component {
    constructor(props) {
        super(props);
        this.mapView = null;
        this.state = {
            routeModal: false,
            name: '',
            description: '',
            locationInterval: null,
            error: '',
            latitude: null,
            longitude: null,
            fitPosition: true
        }
    }

    componentDidMount = async () => {
        console.log("Wtf")
        this.setCurrentPositionOnMap()
    }

    saveRouteModal(visible) {
        this.setState({ routeModal: visible });
    }

    setCurrentPositionOnMap = () => {
        this.watchID = navigator.geolocation.watchPosition((position) => {
            this.props.store.setMyPosition({
                latitude: parseFloat(position.coords.latitude),
                longitude: parseFloat(position.coords.longitude),

                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            })
            this.setState({ latitude: position.coords.latitude, longitude: position.coords.longitude })
            if (this.state.fitPosition) {
                this.fitToMyPosition()
                this.setState({ fitPosition: false })
            }
        },
            (error) => { console.log(JSON.stringify(error)); alert(JSON.stringify(error)) },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 1 },
        )


        const locationInterval = setInterval(() => navigator.geolocation.getCurrentPosition((position) => {
            console.log("currenct", position)
            this.props.store.setMyPosition({
                latitude: parseFloat(position.coords.latitude),
                longitude: parseFloat(position.coords.longitude),
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            })
            this.setState({ latitude: position.coords.latitude, longitude: position.coords.longitude })
            if (this.state.fitPosition) {
                this.fitToMyPosition()
                this.setState({ fitPosition: false })
            }
            navigator.geolocation.clearWatch(this.watchID)

        },

            (error) => { console.log(JSON.stringify(error)); alert(JSON.stringify(error)) },
            { enableHighAccuracy: true, timeout: 200, maximumAge: 0 }), 1000)
        this.setState({ locationInterval })

        // this.fitToMyPosition({
        //     latitude: parseFloat(position.coords.latitude),
        //     longitude: parseFloat(position.coords.longitude),

        //     latitudeDelta: MY_POSITION_LATITUDE,
        //     longitudeDelta: MY_POSITION_LONGITUDE
        // })
    }

    fitToMyPosition = () => {
        // if (this.mapView) {
        //     setTimeout(async () => {
        this.mapView.animateToRegion({
            latitude: parseFloat(this.state.latitude),
            longitude: parseFloat(this.state.longitude),

            latitudeDelta: MY_POSITION_LATITUDE,
            longitudeDelta: MY_POSITION_LONGITUDE
        })
        //     }, 1000);
        // }, 300)
        // }

    }

    getClose = async () => {
        if (this.props.store.routeDistance.distance.toFixed(2) < 0.2) {
            const soundObject = new Audio.Sound();
            try {
                await soundObject.loadAsync(require('../components/notiffication.mp3'));
                await soundObject.playAsync();
            } catch (error) {

            }
            this.props.navigation.navigate("MapMarkerView")
        }
    }

    componentWillUnmount() {
        if (this.state.locationInterval) {
            clearInterval(this.state.locationInterval)
        }
    }

    static navigationOptions = ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
        swipeEnabled: false
    });

    changeValue = () => {
        this.setState({ routeModal: false })
    }

    saveRouteToDB = () => {
        // this.props.store.saveRoute()
        if (this.state.name === "" || this.state.description === "") {
            this.setState({ error: 'You have to set name and description for your route' })
        } else {
            db = firebase.firestore()
            db.collection("routes").add({
                route: this.props.store.clickedMarkers,
                name: this.state.name,
                description: this.state.description
            })
                .then(function (docRef) {
                    console.log("Document written with ID: ", docRef.id);
                })
                .catch(function (error) {
                    console.error("Error adding document: ", error);
                })
            this.setState({ routeModal: false, name: '', description: '', error: '' })
            this.props.store.clearRoute()

        }
    }

    setName = ({ name }) => {
        this.setState({ name: name })
    }

    setDescription = ({ description }) => {
        this.setState({ description: description })
    }


    render() {
        const { store: { clickedMarkers, myPosition, markerList, getMapMarkerDetails, routeDistance, onMarkerClick, clearRoute, saveRoute, setRouteDistance } } = this.props

        return (
            <View style={{ position: 'relative' }}>
                {routeDistance && <View style={styles.distanceView}>
                    <View>

                        <Text>Distance: {routeDistance.distance.toFixed(2)}km</Text>
                        <Text>Duration: {routeDistance.duration.toFixed(2)}min</Text>
                    </View>
                </View>}
                {clickedMarkers.length > 0 &&
                    <TouchableOpacity onPress={() => this.setState({ routeModal: true })} style={styles.saveRouteButton}>
                        <MaterialIcons name="save" color="red" size={25} />
                    </TouchableOpacity>
                }

                <MapView
                    initialRegion={{
                        latitude: LATITUDE,
                        longitude: LONGITUDE,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                    }}
                    style={{ width: '100%', height: '100%' }}
                    ref={c => this.mapView = c}
                >
                    {myPosition && <Marker
                        coordinate={myPosition}
                    >
                        <View style={styles.myLocation}>
                            <View style={styles.myLocation2} />
                        </View>
                    </Marker>}

                    <TouchableOpacity
                        style={styles.modalButton}
                        onPress={() => this.props.navigation.navigate("Categories")}
                    >
                        <SimpleLineIcons name="options-vertical" color="black" size={20} />
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.clearButton}
                        onPress={clearRoute}

                    >
                        <MaterialIcons name="clear" color="black" size={25} />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.myLocationButton} onPress={() => this.fitToMyPosition()}>
                        <MaterialIcons name="my-location" color="black" size={25} />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.historyButton} onPress={() => this.props.navigation.navigate("History")}>
                        <MaterialIcons name="history" color="blue" size={25} />
                    </TouchableOpacity>

                    {markerList.map(item => (
                        <Marker
                            onPress={() => { getMapMarkerDetails(item); this.props.navigation.navigate("MapMarkerView") }}
                            coordinate={item}
                        >
                            <Ionicons name="ios-pin" color="orange" size={40} />
                        </Marker>
                    ))}

                    {clickedMarkers.map(item => (
                        <MapView.Marker
                            coordinate={item}
                        >
                            <Ionicons name="ios-pin" color="red" size={40} />

                        </MapView.Marker>
                    ))}

                    {/* {this.props.store.markerList.map((coordinate, index) =>
                    <MapView.Marker key={`coordinate_${index}`} coordinate={coordinate} />
                )}
                {this.props.store.markerList.length >= 2 && (

                )} */}
                    {
                        clickedMarkers.length > 0 &&
                        <MapViewDirections
                            origin={myPosition}
                            waypoints={clickedMarkers.length > 1 ? clickedMarkers.slice(0, clickedMarkers.length - 1) : null}
                            destination={clickedMarkers[clickedMarkers.length - 1]}
                            apikey={GOOGLE_MAPS_APIKEY}
                            strokeWidth={3}
                            strokeColor="red"
                            optimizeWaypoints={true}
                            onReady={result => {
                                setRouteDistance({ distance: result.distance, duration: 4 * result.duration })
                            }}
                        />
                    }



                </MapView>

                {/* <SaveRouteModal
                    changeValue={() => this.changeValue(this.state)}
                    onRequestClose={() => {
                        this.saveRouteModal(!this.state.routeModal)
                    }}
                    routeModal={this.state.routeModal}
                    name={this.state.name}
                    description={this.state.description}
                    changeName={(name) => this.setName(name)}
                    changeDescription={(description) => this.setDescription(description)}
                    saveRoute={() => this.saveRouteToDB()}
                /> */}

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.routeModal}
                    onRequestClose={() => {
                        this.saveRouteModal(!this.state.routeModal)
                    }}
                >
                    <TouchableOpacity style={styles.modal1} onPress={() => this.setState({ routeModal: false })} activeOpacity={1}>
                        <View style={styles.modal2}>
                            <TextInput
                                style={styles.input}
                                onChangeText={(name) => this.setState({ name })}
                                value={this.state.name}
                                placeholder="Route Name"
                                placeholderTextColor="#fff"
                            />

                            <TextInput
                                style={styles.input}
                                onChangeText={(description) => this.setState({ description })}
                                value={this.state.description}
                                placeholder="Description"
                                placeholderTextColor="#fff"
                                multiline={true}
                            />

                            {
                                this.state.error !== '' && (
                                    <View style={{ width: '90%', alignItems: 'center' }}>
                                        <Text style={{ color: 'red' }}>{this.state.error}</Text>
                                    </View>
                                )
                            }

                            <TouchableOpacity
                                style={styles.modalButton2}
                                onPress={() => this.saveRouteToDB()}
                            >
                                <Text style={styles.modalButtonText}>Save to History</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                </Modal>
            </View>

        );
    }
}
