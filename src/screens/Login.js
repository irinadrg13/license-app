import { View, Text, TouchableOpacity, ImageBackground, Modal, TextInput } from 'react-native'
import React, { Component, Fragment } from 'react'
import styles from "../styles/LoginStyles";
import { createUser, login } from '../modules/firebaseAPI'
import firebase from 'firebase'

export default class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            signInModalVisible: false,
            signUpModalVisible: false,
            emailLogin: '',
            emailCreateAccount: '',
            passwordLogin: '',
            passwordCreateAccount: '',
            errorMessageCreateAccount: null,
            errorMessageLogin: null
        }
    }

    static navigationOptions = ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
        swipeEnabled: false
    })

    signInModal(visible) {
        this.setState({ signInModalVisible: visible });
    }

    signIn = async () => {
        const { emailLogin, passwordLogin } = this.state
        firebase
            .auth()
            .signInWithEmailAndPassword(emailLogin, passwordLogin)
            .then(() => {
                this.props.navigation.navigate('Map');
                this.signInModal(!this.state.signInModalVisible)
            })
            .catch(error => this.setState({ errorMessageLogin: error.message }))
    }

    signUpModal(visible) {
        this.setState({ signUpModalVisible: visible });
    }

    createNewAccount = async () => {
        const { emailCreateAccount, passwordCreateAccount } = this.state
        firebase
            .auth()
            .createUserWithEmailAndPassword(emailCreateAccount, passwordCreateAccount)
            .then(user => {
                this.props.navigation.navigate('Map');
                this.signUpModal(!this.state.signUpModalVisible)
            })
            .catch(error => this.setState({ errorMessageCreateAccount: error.message }))
    }

    render() {
        return (
            <ImageBackground style={styles.container} source={require("../images/Iasi.png")}>
                <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 1, paddingVertical: 10, width: '100%' }}>
                    <TouchableOpacity
                        style={styles.loginButton}
                        onPress={() => {
                            this.signInModal(true)
                        }}
                    >
                        <Text style={styles.loginButtonText}>Login</Text>
                    </TouchableOpacity>


                    <TouchableOpacity
                        style={styles.loginButton}
                        onPress={() => {
                            this.signUpModal(true);
                        }}
                    >
                        <Text style={styles.loginButtonText}>Create new account</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.loginButton}
                        onPress={() => {
                            this.props.navigation.navigate("IasiDetails");
                        }}
                    >
                        <Text style={styles.loginButtonText}>About Iasi</Text>
                    </TouchableOpacity>
                </View>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.signInModalVisible}
                    onRequestClose={() => {
                        this.signInModal(!this.state.signInModalVisible)
                    }}>
                    <TouchableOpacity style={styles.modal1} onPress={() => this.setState({ signInModalVisible: false })} activeOpacity={1}>
                        <View style={styles.modal2}>
                            <TextInput
                                style={styles.input}
                                onChangeText={(emailLogin) => this.setState({ emailLogin })}
                                value={this.state.emailLogin}
                                placeholder="Email"
                                placeholderTextColor="#fff"
                                keyboardType="email-address"
                            />

                            <TextInput
                                style={styles.input}
                                onChangeText={(passwordLogin) => this.setState({ passwordLogin })}
                                value={this.state.passwordLogin}
                                placeholder="Password"
                                placeholderTextColor="#fff"
                                secureTextEntry={true}
                            />
                            {this.state.errorMessageLogin &&
                                <View style={{ width: '90%', alignItems: 'center' }}>
                                    <Text style={{ color: 'red' }}>
                                        {this.state.errorMessageLogin}
                                    </Text>
                                </View>}

                            <TouchableOpacity
                                style={styles.modalButton}
                                onPress={() => this.signIn()}
                            >
                                <Text style={styles.modalButtonText}>Login</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.signUpModalVisible}
                    onRequestClose={() => {
                        this.signUpModal(!this.state.signUpModalVisible)
                    }}>
                    <TouchableOpacity style={styles.modal1} onPress={() => this.setState({ signUpModalVisible: false })} activeOpacity={1}>
                        <View style={styles.modal2}>
                            <TextInput
                                style={styles.input}
                                onChangeText={(emailCreateAccount) => this.setState({ emailCreateAccount })}
                                value={this.state.emailCreateAccount}
                                placeholder="Email"
                                placeholderTextColor="#fff"
                                keyboardType="email-address"
                            />

                            <TextInput
                                style={styles.input}
                                onChangeText={(passwordCreateAccount) => this.setState({ passwordCreateAccount })}
                                value={this.state.passwordCreateAccount}
                                placeholder="Password"
                                placeholderTextColor="#fff"
                                secureTextEntry={true}
                            />

                            {this.state.errorMessageCreateAccount &&
                                <Text style={{ color: 'red' }}>
                                    {this.state.errorMessageCreateAccount}
                                </Text>}

                            <TouchableOpacity
                                style={styles.modalButton}
                                onPress={() => this.createNewAccount()}
                            >
                                <Text style={styles.modalButtonText}>Sign up</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                </Modal>
            </ImageBackground>
        )
    }
}