import React, { Component } from 'react'
import { Dimensions, StyleSheet, View, Modal, ScrollView, Text, TouchableOpacity, Linking, Image, ActivityIndicator } from 'react-native';
import { inject, observer } from 'mobx-react/native';
import Geocode from 'react-geocode';
import 'firebase/firestore'
import firebase from 'firebase'
import styles from '../styles/MapMarkerViewStyles'

Geocode.setApiKey('AIzaSyCTRVGqebWdSAUYdBP9jXYgSfIewJMyibM');
@inject('store')
@observer
export default class MapMarkerView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            address: '',
            description: '',
            link: '',
            name: '',
            image: ''
        }
    }

    componentDidMount() {
        // console.log(this.props.store.MapMarkerDetails, "marker details");
        Geocode.fromLatLng(this.props.store.MapMarkerDetails.latitude, this.props.store.MapMarkerDetails.longitude).then(
            response => {
                const address = response.results[0].formatted_address;
                this.setState({ address: address })
                console.log(this.state.address);
            },
            error => {
                console.error(error);
            }
        );
        console.log(this.props.store.MapMarkerDetails.id);

        db = firebase.firestore()
        var docRef = db.collection("museums").doc(this.props.store.MapMarkerDetails.id);

        docRef.get().then((doc) => {
            if (doc.exists) {
                console.log(doc.data());

                this.setState({
                    description: doc.data().description,
                    link: doc.data().link,
                    name: doc.data().name,
                    image: doc.data().image
                })
            } else {
                console.log("No such document!");
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });
    }

    static navigationOptions = ({ navigation }) => ({
        header: null
    })

    onNavigatePress = (item) => {
        this.props.store.onMarkerClick(item)
        this.props.navigation.navigate("Map")
    }

    ReadMore = () => {
        Linking.openURL(this.state.link)
    }

    render() {

        const { store: { clickedMarkers, myPosition, markerList, getMapMarkerDetails, routeDistance, onMarkerClick, clearRoute, saveRoute, setRouteDistance, MapMarkerDetails } } = this.props
        // console.log(MapMarkerDetails)
        return (
            <View style={{ flex: 1, paddingTop: 30, backgroundColor: 'rgba(0,0,0,0.7)' }}>
                <ScrollView>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ fontSize: 18, marginLeft: 15, height: 40, width: 100, borderRadius: 20, backgroundColor: 'rgba(255,255,255,0.7)', justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                        <Text>Back to map</Text>
                    </TouchableOpacity>

                    {
                        this.state.image !== '' ?
                            (
                                <Image
                                    source={{ uri: this.state.image }}
                                    style={{ width: '80%', alignSelf: 'center', height: 200 }}
                                />
                            ) : (
                                <ActivityIndicator />
                            )
                    }

                    <Text style={styles.details}>
                        {this.state.name}
                    </Text>

                    <Text style={styles.details}>
                        {this.state.description}
                    </Text>

                    <Text style={styles.details}>
                        Address: {this.state.address}
                    </Text>
                </ScrollView>
                <View style={styles.buttonsView}>
                    <TouchableOpacity style={styles.buttons}
                        onPress={() => this.ReadMore()}>
                        <Text style={styles.buttonsText}>Read more</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.buttons}
                        onPress={() => this.onNavigatePress(MapMarkerDetails)}>
                        <Text style={styles.buttonsText}>Navigate</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
