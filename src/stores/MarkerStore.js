import { observable, action } from 'mobx'
import { Dimensions } from 'react-native'
import uuidv4 from 'uuid/v4'
import axios from 'axios'
import 'firebase/firestore'
import firebase from 'firebase'

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 47.1593295
const LONGITUDE = 27.5674332
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const GOOGLE_MAPS_APIKEY = 'AIzaSyCTRVGqebWdSAUYdBP9jXYgSfIewJMyibM';

class MarkerStore {
    @observable coordinates = [
        {
            latitude: 47.185125400000004,
            longitude: 27.55339825633581,
        },
        {
            latitude: 47.1573066,
            longitude: 27.5869643504902,
        },
    ]
    @observable clickedMarkers = []
    @observable markerList = []
    @observable myPosition = null
    @observable savedRoutes = []
    @observable routeDistance = null
    @observable MapMarkerDetails = { latitude: 0, longitude: 0, id: 0 }
    @action getMarkers({ type, success }) {
        axios({
            url: `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=47.1551537%2C27.5938661&radius=15000&type=${type}&key=${GOOGLE_MAPS_APIKEY}`,
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(this.onMarkersSuccess)
            .catch(this.onMarkersFail)
    }
    @action.bound
    onMarkersSuccess(response) {
        this.markerList = response.data.results.map(item =>
            ({ id: item.place_id, latitude: item.geometry.location.lat, longitude: item.geometry.location.lng, name: item.name }))
    }

    setRouteDistance = (routeDistance) => {
        this.routeDistance = { ...routeDistance }
    }

    onMarkerClick = (marker) => {
        let newClickedMarkers = [...this.clickedMarkers]
        if (this.clickedMarkers.filter(item => item.id === marker.id).length > 0) {
            newClickedMarkers = this.clickedMarkers.filter(item => item.id !== marker.id)
            newClickedMarkers.push(marker)
        } else {
            newClickedMarkers.push(marker)
            this.removeMarkerFromMarkerList(marker)
        }
        this.clickedMarkers = newClickedMarkers
    }

    clearRoute = () => {
        this.markerList = [...this.markerList, ...this.clickedMarkers]
        this.clickedMarkers = []
        this.routeDistance = null
    }

    viewRoute = (route) => {
        this.clickedMarkers = [...route]
    }

    saveRoute = () => {
        // this.savedRoutes = [...this.savedRoutes, { id: uuidv4(), route: [...this.clickedMarkers] }]
        console.log(this.clickedMarkers);

        db = firebase.firestore()
        db.collection("routes").add({
            route: this.clickedMarkers
        })
            .then(function (docRef) {
                console.log("Document written with ID: ", docRef.id);
            })
            .catch(function (error) {
                console.error("Error adding document: ", error);
            })

        this.clearRoute()
    }

    removeMarkerFromMarkerList = (marker) => {
        this.markerList = this.markerList.filter(item => item.id !== marker.id)
    }

    getMapMarkerDetails = async (item) => {
        // let response
        // try {

        //     response = await axios({
        //         url: `https://maps.googleapis.com/maps/api/place/details/json?placeid=${item.id}&key=${GOOGLE_MAPS_APIKEY}&language=en`,
        //         method: "get"
        //     })
        //     console.log(response.data, "details")
        // } catch (e) {
        //     console.log(e)
        // }
        this.MapMarkerDetails = { ...item }
    }
    setMyPosition = (position) => {
        this.myPosition = { ...position }
    }


}

const markerStore = new MarkerStore()
export default markerStore