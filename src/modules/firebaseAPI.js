import firebase from 'firebase'

// export const createUser = (email, password) => {
//     firebase.auth().createUserWithEmailAndPassword(email, password).catch((error) => console.log("Create User Error: ", error))
// }

export const createUser = (email, password) => {
    firebase.auth().createUserWithEmailAndPassword(email, password)
        .catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode == 'auth/weak-password') {
                alert('The password is too weak.');
            } else {
                alert(errorMessage);
            }
            console.log(error);
        });
}

export const login = (email, password) => {
    firebase.auth().signInWithEmailAndPassword(email, password)
        .catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode === 'auth/wrong-password') {
                alert('Wrong password.');
            } else {
                alert(errorMessage);
            }
            console.log(error);
        });
}

export const logout = () => {
    firebase.auth().signOut().catch((error) => console.log(error))
}


export default addDataToFirestore = () => {
    var db = firebase.firestore()
    db.collection("users").add({
        first: "Ada",
        last: "Lovelace",
        born: 1815
    })
        .then(function (docRef) {
            console.log("Document written with ID: ", docRef.id);
        })
        .catch(function (error) {
            console.error("Error adding document: ", error);
        });
}