import { Modal, TouchableOpacity, TextInput, View, Text } from 'react-native'
import React from 'react'
import styles from '../styles/MapStyles'

export const SaveRouteModal = ({ changeValue, onRequestClose, routeModal, name, description, changeName, changeDescription, saveRoute }) => {
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={routeModal}
            onRequestClose={onRequestClose}
        >
            <TouchableOpacity style={styles.modal1} onPress={changeValue} activeOpacity={1}>
                {/* <View style={styles.modal1}> */}
                <View style={styles.modal2}>
                    <TextInput
                        style={styles.input}
                        onChangeText={changeName}
                        value={name}
                        placeholder="Route Name"
                        placeholderTextColor="#fff"
                    />

                    <TextInput
                        style={styles.input}
                        onChangeText={changeDescription}
                        value={description}
                        placeholder="Description"
                        placeholderTextColor="#fff"
                    />

                    <TouchableOpacity
                        style={styles.modalButton2}
                        onPress={saveRoute}
                    >
                        <Text style={styles.modalButtonText}>Save to History</Text>
                    </TouchableOpacity>
                    {/* </View> */}
                </View>
            </TouchableOpacity>
        </Modal>
    )
}