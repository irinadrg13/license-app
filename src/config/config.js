import firebase from 'firebase'

export const initializeFirebase = () => {
    const firebaseConfig = {
        apiKey: "AIzaSyAouBYXCCUPmKEvxQ0mCNU1P22tLZUubdA",
        authDomain: "license-10fd8.firebaseapp.com",
        databaseURL: "https://license-10fd8.firebaseio.com",
        projectId: "license-10fd8",
        storageBucket: "license-10fd8.appspot.com",
        messagingSenderId: "196573024524",
        appId: "1:196573024524:web:17f86e1350853cd0"
    }
    firebase.initializeApp(firebaseConfig)
}

